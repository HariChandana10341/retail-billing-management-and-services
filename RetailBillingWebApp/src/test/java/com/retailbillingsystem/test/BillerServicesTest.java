package com.retailbillingsystem.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.retailbillingsystem.services.BillerServices;

/**
 * This class is created to test all possible scenarios in manager services
 * @author Batch A
 *
 */
public class BillerServicesTest {
	
	BillerServices billeservices = null;
	
	@Before
	public void setUp()
	{
		billeservices = new BillerServices();
	}
	
	@After
	public void tearDown()
	{
		billeservices = null;
	}
	
	
	@Test
	public void price_calculationTestPositive()
	{
		assertEquals(660, (int)billeservices.price_calculation("sugar", 12));
	}
	
	@Test
	public void price_calculationTestNegative()
	{
		assertEquals(0,(int)billeservices.price_calculation("santoor", 12));
	}
	
	@Test
	public void total_calculationPositive()
	{
		assertEquals(55,(int)billeservices.total_calculation(43, 12));
	}
	
	@Test
	public void total_calculationNegative()
	{
		assertNotEquals(555555655,billeservices.total_calculation(43, 12));
	}
	
	
}
