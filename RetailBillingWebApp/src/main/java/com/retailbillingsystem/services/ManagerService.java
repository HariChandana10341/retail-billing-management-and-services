package com.retailbillingsystem.services;


import java.util.List;

import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.dao.EmployeeDao;

/**
 * This class is used to allow manager to perform his operations
 * @author Batch-A
 *
 */
public class ManagerService {

	/**
	 * This method invokes createEmployee method in WritingData class to create a stocker
	 * @param name
	 * @param pwd
	 * @param dish
	 * @return returns true if employee is created successfully
	 */
	public String createEmployee(EmployeeDetails employeeDetails)
	{
		String ret = "";
		if(employeeDetails.getRole().equalsIgnoreCase("Manager"))
		{
			ret="You have no access to add a manager";
		}
		else
		{
			EmployeeDao insertEmployee=new  EmployeeDao();
			boolean flag = insertEmployee.createEmployee(employeeDetails);
			if(flag)
			{
				ret = "Employee added successfully";
			}
			else
			{
				ret = "Addition failed";
			}
		}
		
		return ret;
		
	}
	
	/**
	 * This method invokes delete employee method in ManipulatingData class to delete employee
	 * @param id
	 * @param role
	 * @return true if employee is deleted successfully
	 */
	public boolean deleteEmployee(int id){
		if(id==1)
		{
			return false;
		}
		List<EmployeeDetails> details = EmployeeDao.fetchEmployeeDetails();
		EmployeeDetails employeeDetail= new EmployeeDetails(); 
		for(EmployeeDetails employeeDetails : details)
		{
			if(employeeDetails.getEmployeeId() == id)
			{
				employeeDetail.setEmployeeId(id);
				employeeDetail.setEmployeeName(employeeDetails.getEmployeeName());
				employeeDetail.setEmployeePassword(employeeDetails.getEmployeePassword());
				employeeDetail.setRole(employeeDetails.getRole());
				employeeDetail.setSecurityQuestion(employeeDetails.getSecurityQuestion());
				
			}
		}
		EmployeeDao data = new EmployeeDao();
		return data.deleteEmployee(employeeDetail);
		
	}
	
	/*******
	 * This method is used to password validation of employee
	 * @param password
	 * @return true if password format is correct
	 */
	public static boolean passwordChecking(String password) {
		int Specialcount=0;
		
		
		for(int i=0;i<password.length();i++) {
			if((password.charAt(i)>=48 && password.charAt(i)<=57) ) {
				Specialcount++;
			}
			
			
	}
		if(Specialcount>0) {
			return true;
		}
		return false;
}
	
	/**
	 * This method invokes fetchEmployee details
	 * @return list of EmployeeDetails
	 */
	public List<EmployeeDetails> displayEmployees(){
		List<EmployeeDetails> employeelist =EmployeeDao.fetchEmployeeDetails();
		return  employeelist;
	}
}
