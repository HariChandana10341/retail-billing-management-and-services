package com.retailbillingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.retailbillingsystem.bean.FeedBack;


/**
 * This class is used to perform feedback DB operations
 * @author Batch-A
 *
 */
public class FeedbackDao {

	/**
	 * This method is used to fetch feedback details
	 * @return list of fedback details
	 */
	public List<FeedBack> fetchFeedBackDetails()
	{
		
		List<FeedBack> feedbackdetails = new ArrayList<FeedBack>();
		
		//returning list
		return feedbackdetails;
		
    }
	
	/**
	 * This method is used for inserting the feedback of customer. This is stored whenever the customer gives the feedabck.
	 * @param customerfeedback
	 */
	public boolean feedbackDetails(FeedBack customerfeedback)
	{
		boolean result=false;
		Connection con = null;
		try {
			//calling the method to establish connection
			con = DBUtil.getconn();
			
			//executing insert query by using prepared statement 
			String sql="insert into customerfeedback_tab(date_time,feedback,cst_mbn) values(?,?,?)";
			PreparedStatement pst= con.prepareStatement(sql);
			
			//setting the values into table
			pst.setString(1,customerfeedback.getDateandtime());
			if(customerfeedback.getFeedback()==1) {
				pst.setString(2,"very good");
			}
			else if(customerfeedback.getFeedback()==2) {
				pst.setString(2, "good");
			}
           else if(customerfeedback.getFeedback()==3) {
        	   pst.setString(2, "average");
			}
           else if(customerfeedback.getFeedback()==4) {
        	   pst.setString(2, "poor");
			}
           else {
        	   pst.setString(2, " very poor");
           }
			pst.setString(3, customerfeedback.getMobileNumber());
			
			int rowsEffected=pst.executeUpdate(); 
			if(rowsEffected>0)
			{
				result=true;
			}
						
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	
	}
	
}
