package com.retailbillingsystem.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.retailbillingsystem.bean.Billing_details;
import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.util.HibernateUtil;


/**
 * This class is used to perform invoice DB operations
 * @author Batch-A
 *
 */
public class InvoiceDao {

	/**
	 * This method is used to fetch invoices
	 * @return list of invoices
	 */
	public static List<Invoice_details> readFromTable_invoice()
	{
		
		List<Invoice_details> invoiceDetails = new ArrayList<Invoice_details>();

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Invoice_details ");
		
		invoiceDetails = query.getResultList();
		
		
		//returning list
		return invoiceDetails;

		
	}
	
	/**
	 * This method is used to insert previous bill details into database. This is used whenever the bill is generated.
	 * @param invoice_num
	 * @param total
	 * @param discount
	 * @param tax
	 */
	public void invoiceDetails(Billing_details billing_details) {
		
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
				
		Transaction txn = session.beginTransaction();
		
		session.save(billing_details);
		
		
		txn.commit();
		
		session.close();
		
		
	}
	
	/**
	 * This method is used to fetch invoice numbers
	 * @return list of invoice numbers
	 */
	public  static List<Integer> readFromTable_InvoiceNumbers() 
	{
		
		
		List<Integer> invoiceNumbers = new ArrayList<Integer>();

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("distinct Invoiceno from Invoice_details ");
		
		invoiceNumbers = query.getResultList();
		
		
		//returning list
		return invoiceNumbers;
		
	}
}
