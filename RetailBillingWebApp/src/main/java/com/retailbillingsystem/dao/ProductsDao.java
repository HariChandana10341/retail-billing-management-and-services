package com.retailbillingsystem.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.retailbillingsystem.util.HibernateUtil;
import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.bean.Products;


/**
 * This class is used to perform Products DB operations
 * @author Batch-A
 *
 */
public class ProductsDao {
	/**
	 * This method is used to fetch products
	 * @return list of products
	 */
	public static List<Products> readFromTable_products()
	{
		
		List<Products> products = new ArrayList<Products>();
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Products");
		products=query.getResultList();
		
		
		return products;
		
	}
	
	/**
	 * This method is used to fetch billing products
	 * @return list of products
	 */
	public static List<Invoice_details> readFromTable_billingProducts(int invoice_number)
	{
		List<Invoice_details> billingProducts = new ArrayList<Invoice_details>();
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Invoice_details where invoiceno=?");
		query.setParameter(0, invoice_number);
		billingProducts = query.getResultList();
		
		
		//returning list
		return billingProducts;
	}
	
	
	/**
	 * This method is used for deleting the product when required. This is a stocker operation.
	 * @param productName
	 * @return true if product deleted successfully else false
	 */
	public boolean deleteFromTable_products(Products products) {
		
		//calling a method to establish connection

		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		
		session.delete(products);
		txn.commit();
		return true;
	}

	/**
	 * This method is used for updating quantity of a particular product
	 * @param productName
	 * @param quantity
	 * @return true if product's quantity updated successfully else false
	 */
	public boolean updateTable_products_quantity(Products products) {

	
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		
		session.update(products);
		txn.commit();
		return true;
	}
	/**
	 * This method is used for updating price of a particular product
	 * @param productName
	 * @param price
	 * @return true if product's price updated successfully else false
	 */
	public boolean updateTable_products_price(Products products) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		
		session.update(products);
		txn.commit();
		return true;
	}
	
	/**
	 * This is used for inserting product data into table. This is a stocker operation
	 * @param productname
	 * @param quantity
	 * @param price
	 * @param productType
	 */
	public boolean productsInsertion(Products products)
	{
		boolean flag = false;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
				
		Transaction txn = session.beginTransaction();
		
		session.save(products);
		flag = true;
		
		txn.commit();
		
		session.close();
		return flag;
		
	}
	/**
	 * This is used for inserting product data into table while generating the bill. This is a biller operation.
	 * @param invoiceList
	 */
	public void billingProductsInsertion(ArrayList<Invoice_details> invoiceList) {
		
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
				
		Transaction txn = session.beginTransaction();
		for (Invoice_details details:invoiceList) {
			session.save(details);
		}
		
				
		txn.commit();
		
		session.close();
		
	}
}
