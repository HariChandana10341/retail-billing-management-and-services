package com.retailbillingsystem.provider;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.bean.FeedBack;
import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.dao.FeedbackDao;
import com.retailbillingsystem.dao.InvoiceDao;
import com.retailbillingsystem.dao.ProductsDao;
import com.retailbillingsystem.services.ManagerService;
/**
 * this class is used for manager to access his services
 * @author BatchA
 *
 */
@Path("/manager")
public class ManagerProvider {
	
	
	/**
	 * this method is used to add employees data 
	 * @param employeeDetails
	 * @return String
	 */
	@POST
	@Path("/addEmployee")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addEmployee(EmployeeDetails employeeDetails)
	{
		//creating object for manager services and calling create employee method
		ManagerService managerService = new ManagerService();
		String res = managerService.createEmployee(employeeDetails);
		
		
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(res).build();
	}
	/**
	 * this method is used to delete employees data 
	 * @param employeeDetails
	 * @return String
	 */
	@DELETE
	@Path("/delEmployee")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response delEmployee(EmployeeDetails employeeDetails) 
	{
		//creating object for manager services and calling delete employee method
		ManagerService managerService = new ManagerService();
		boolean res = managerService.deleteEmployee(employeeDetails.getEmployeeId());

		//storing the output from the method and returning as JSON object
		String response =  (res ? "Deleted Successfully" : "Deletion failed");
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(response).build();
	}
	/**
	 * this method is used to display employees data
	 * @return String
	 */
	@GET
	@Path("/displayEmployee")
	@Produces(MediaType.APPLICATION_JSON)
	public Response displayEmployee()
	{
		//creating object for manager services and calling display employee method
		ManagerService managerService = new ManagerService();
		List<EmployeeDetails> employeeList = managerService.displayEmployees();

		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(employeeList).build();
	}
	/**
	 * this method is used to display invoice details of previous bills  
	 * @param Invoice_details
	 * @return String
	 */
	@GET
	@Path("/displaybills")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response displaypreviousBills(Invoice_details invoice)
	{
		int invoiceno = invoice.getInvoiceno();
		
		//calling the readfromtable method and storing the data into a list
		List<Integer>invoicenumbers = InvoiceDao.readFromTable_InvoicenNumbers();
		int count=0;
		//creating a arraylist object
		ArrayList invoiceList = new ArrayList();
		
		//creating invoice number generation
		for(int i=0;i<invoicenumbers.size();i++)
		 {
			 if(invoiceno==invoicenumbers.get(i))
			 {
				count++;
			 }
		 }
		//storing the output into list
		 if(count==0)
		 {
			 invoiceList.add("invoice number not found");
		 }
		 else
		 {
			 invoiceList = (ArrayList) ProductsDao.readFromTable_billingProducts(invoiceno);
		 }
		 //returning the result list
		 return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(invoiceList).build();
	}
	/**
	 * this method is used to display feedback of previous bills  
	 * @return List of feedback data
	 */
	@GET
	@Path("/displayfeedback")
	@Produces(MediaType.APPLICATION_JSON)
	public Response displayFeedback()
	{
		
		FeedbackDao feedbackDao = new FeedbackDao();
		//calling the feeback details table and storing into list
		List<FeedBack> feedBacks = feedbackDao.fetchFeedBackDetails();
		 //returning the result list
		 return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(feedBacks).build();
	}
}