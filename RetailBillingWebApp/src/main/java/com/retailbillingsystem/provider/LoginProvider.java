package com.retailbillingsystem.provider;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.services.LoginServices;
import com.retailbillingsystem.util.ForgotUtilites;

/**
 * This class is used to allow the employees to get logged in 
 * @author BatchA
 *
 */

@Path("/login")
public class LoginProvider {

	
	/**
	 * this method is used to call  managerLogin method from LoginServices class
	 * @param username
	 * @param password
	 * @return JSON object
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/managerLogin")
	@GET
	public Response managerLogin(EmployeeDetails employeeDetails) 
	{
		//calling managerLogin method inside a login services and storing the result into a boolean
		boolean managerLogin = new LoginServices().managerLogin(employeeDetails.getEmployeeName(), employeeDetails.getEmployeePassword());
		String res = managerLogin ? "Login successfull...." : "please enter valid details";
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(res).build();
	}
	
	/**
	 * this method is used to call  stockerLogin method from LoginServices class
	 * @param username
	 * @param password
	 * @return JSON object
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/stockerLogin")
	@GET
	public Response stockerLogin(EmployeeDetails employeeDetails)  {
		
		//calling stockerLogin method inside a login services and storing the result into a boolean
		boolean stockerLogin = new LoginServices().stockerLogin(employeeDetails.getEmployeeName(), employeeDetails.getEmployeePassword());
		String res = stockerLogin ? "Login successfull...." : "please enter valid details";
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(res).build();
	}
	
	/**
	 * this method is used to call  billerLogin method from LoginServices class
	 * @param username
	 * @param password
	 * @return JSON object
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/billerLogin")
	@GET
	public Response billerLogin(EmployeeDetails employeeDetails)  {
		
		//calling billerLogin method inside a login services and storing the result into a boolean
		boolean billerLogin = new LoginServices().billerLogin(employeeDetails.getEmployeeName(), employeeDetails.getEmployeePassword());
		String res = billerLogin ? "Login successfull...." : "please enter valid details";
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(res).build();
	}
	
	/**
	 * This method is used to provide implementation for forgot password
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/forgotPassword")
	@GET
	public Response forgotPassword(EmployeeDetails employeeDetails)
	{
		//Calling method of forgot password
		String password = ForgotUtilites.forgotPasswod(employeeDetails);
		
		if(password=="")
		{
			password =  "Try again after some time.....";
		}
		
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(password).build();
	}
}
