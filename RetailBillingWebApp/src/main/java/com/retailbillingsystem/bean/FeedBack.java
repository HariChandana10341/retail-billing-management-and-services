package com.retailbillingsystem.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class is created to store customer feedbacks
 * 
 * @author Batch-A
 *
 */
@Entity
@Table(name="customerfeedback_tab")
public class FeedBack {

	// initializing the variables
	@Column(name="feedback")
	private String feedback;
	@Column(name="date_time")
	private String dateandtime;
	@Column(name="cst_mbn")
	private String mobileNumber;

	/***
	 * This method is for getting the mobileNumber of the customer on specified date
	 * 
	 * @return mobileNum
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/***
	 * This method is for setting the mobileNumber when the customer gives it
	 * 
	 * @param mobileNumber
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/***
	 * This method is for getting the feedback of the customer on specified date
	 * 
	 * @return feedback
	 */
	public String getFeedback() {
		return feedback;
	}

	/***
	 * This method is for setting the feedback when the customer gives it
	 * 
	 * @param feedback
	 */
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	/***
	 * This method is fo rgetting the date and time of the particular feedback
	 * 
	 * @return date and time
	 */
	public String getDateandtime() {
		return dateandtime;
	}

	/***
	 * This method is for setting the date and time of the feedback
	 * 
	 * @param dateandtime
	 */
	public void setDateandtime(String dateandtime) {
		this.dateandtime = dateandtime;
	}

}
