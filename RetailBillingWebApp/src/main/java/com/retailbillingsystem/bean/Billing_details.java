package com.retailbillingsystem.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/***
 * This class is an object for storing the bill details according to date of sale
 * @author BATCH A
 *
 */
@Entity
@Table(name="biller_tab")
public class Billing_details {
	
	//initializing the variables
	@Id
	@Column(name="Invoicenumber")
	private int invoiceno;
	
	@Column
	private double tax;
	@Column
	private double discount;
	@Column
	private double total;
	
	/***
	* This method is for getting the tax data of the current bill
	* @return tax
	*/
	public double getTax() {
		return tax;
	}
	
	/***
	* This method is for setting the tax value for the current bill
	* @param tax
	*/
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	/***
	* This method is for getting the discount data of the current bill
	* @return discount
	*/
	public double getDiscount() {
		return discount;
	
	}
	
	/***
	* This method is for setting the discount value for the current bill
	* @param discount
	*/
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	/***
	* This method is for getting the total data of the current bill
	* @return total
	*/
	public double getTotal() {
		return total;
	}
	
	/***
	* This method is for setting the total value for the current bill
	* @param total
	*/
	public void setTotal(double total) {
		this.total = total;
	}
	
	/***
	* This method is for getting the invoiceno data of the current bill
	* @return invoiceno
	*/
	public int getInvoiceno() {
		return invoiceno;
	}
	
	/***
	* This method is for setting the invoiceno value for the current bill
	* @param invoiceno
	*/
	public void setInvoiceno(int invoiceno) {
		this.invoiceno = invoiceno;
	}
	
	
	

	
	
	
}
