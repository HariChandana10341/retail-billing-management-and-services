package com.retailbillingsystem.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/***
 * This class is for storing the previous price details
 * @author BATCH A
 *
 */
@Entity
@Table(name="invoice_tab")
public class Invoice_details {
	
	//initializing the variables
	@Id
	@Column(name = "Invoicenumber")
	private int invoiceno;
	@Column(name = "product_quantity")
	private double quantity;
	@Column(name = "product_price")
	private double price;
	@Id
	@Column(name = "product_name")
	private String productname;
	
	/***
	 * This method is for getting invoiceno of the current bill
	 * @return invoiceno
	 */
	public int getInvoiceno() {
		return invoiceno;
	}
	
	/***
	 * This method is for setting invoiceno of the current bill
	 * @param invoiceno
	 */
	public void setInvoiceno(int invoiceno) {
		this.invoiceno = invoiceno;
	}
	
	/***
	 * This method is for getting quantity of the current bill
	 * @return quantity
	 */
	public double getQuantity() {
		return quantity;
	}
	
	/***
	 * This method is for setting quantity of the current bill
	 * @param quantity
	 */
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	/***
	 * This method is for getting price of the current bill
	 * @return price
	 */
	public double getPrice() {
		return price;
	}
	
	/***
	 * This method is for setting price of the current bill
	 * @param price
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	/***
	 * This method is for getting productname of the current bill
	 * @return productname
	 */
	
	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}
	
	
	
}
