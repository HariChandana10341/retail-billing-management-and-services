package com.retailbillingsystem.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class contains all parameters of product list like
 * product name,price,quantity Also have the add product method ,and display
 * method , delete product
 * 
 * @author Batch A
 */
@Entity
@Table(name="products_tab")
public class Products {
	
	//initializing the variables
	@Id
	@Column(name="product_id")
	private int product_id;
	@Column(name="product_name")
	private String productname;
	@Column(name="product_price")
	private double price;
	@Column(name="product_quantity")
	private double quantity;
	@Column(name="quantity_type")
	private String productType;

	/***
	* This method is for getting the productType of the current product
	* @return productType
	*/
	
	public String getProductType() {
		return productType;
	}

	/***
	* This method is for setting the productType of the current product
	* @param productType
	*/
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/***
	* This method is for getting the product_id of the current product
	* @return product_id
	*/
	public int getProduct_id() {
		return product_id;
	}

	/***
	* This method is for setting the product_id of the current product
	* @param product_id
	*/
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	/***
	* This method is for getting the productname of the current product
	* @return productname
	*/
	public String getProductname() {
		return productname;
	}

	/***
	* This method is for setting the productname of the current product
	* @param productname
	*/
	public void setProductname(String productname) {
		this.productname = productname;
	}

	/***
	* This method is for getting the price of the current product
	* @return price
	*/
	public double getPrice() {
		return price;
	}

	/***
	* This method is for setting the price of the current product
	* @param price
	*/
	public void setPrice(double price) {
		this.price = price;
	}

	/***
	* This method is for getting the quantity of the current product
	* @return quantity
	*/
	public double getQuantity() {
		return quantity;
	}

	/***
	* This method is for setting the quantity of the current product
	* @param quantity
	*/
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Products [productname=" + productname + ", price=" + price + ", quantity=" + quantity
				+ ", getProductname()=" + getProductname() + ", getPrice()=" + getPrice() + ", getQuantity()="
				+ getQuantity() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
