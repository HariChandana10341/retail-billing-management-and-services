package com.retailbillingsystem.services;


import java.util.List;

import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.dao.EmployeeDao;

/**
 * This class is used to allow manager to perform his operations
 * @author Batch-A
 *
 */
public class ManagerService {

	/**
	 * This method invokes createEmployee method in WritingData class to create a stocker
	 * @param name
	 * @param pwd
	 * @param dish
	 * @return returns true if employee is created successfully
	 */
	public boolean createEmployee(EmployeeDetails employeeDetails)
	{
		boolean flag = false;
		EmployeeDao insertEmployee=new  EmployeeDao();
		flag = insertEmployee.createEmployee(employeeDetails);
		return flag;
		
	}
	
	/**
	 * This method invokes delete employee method in ManipulatingData class to delete employee
	 * @param id
	 * @param role
	 * @return true if employee is deleted successfully
	 */
	public boolean deleteEmployee(int id,String role){
		
		EmployeeDao data = new EmployeeDao();
		return data.deleteEmployee(id, role);
		
	}
	
	/*******
	 * This method is used to password validation of employee
	 * @param password
	 * @return true if password format is correct
	 */
	public static boolean passwordChecking(String password) {
		int Specialcount=0;
		
		
		for(int i=0;i<password.length();i++) {
			if((password.charAt(i)>=48 && password.charAt(i)<=57) ) {
				Specialcount++;
			}
			
			
	}
		if(Specialcount>0) {
			return true;
		}
		return false;
}
	
	/**
	 * This method invokes fetchEmployee details
	 * @return list of EmployeeDetails
	 */
	public List<EmployeeDetails> displayEmployees(){
		List<EmployeeDetails> employeelist =EmployeeDao.fetchEmployeeDetails();
		return  employeelist;
	}
}
