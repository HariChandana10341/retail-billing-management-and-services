package com.retailbillingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.util.DBUtil;

/**
 * This class is used to perform employee DB operations
 * @author Batch-A
 *
 */
public class EmployeeDao {
	

	/**
	 * This is used for inserting manager data into table.
	 */
	public void createManager()
	{
		Connection con = null;
		try {
		//calling the method to establish connection
		con = DBUtil.getconn();
		
		//executing insert query by using statement 
		String sql = "insert into Employee_tab values('Manager',1,'Puran','aspire','dosa')";
		Statement st;
		
			st = con.createStatement();
			st.executeUpdate(sql);

		} catch (SQLException e) {
			return;
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * This is used for inserting employee data into table. This is a manager operation
	 * @param role
	 * @param name
	 * @param password
	 * @param answer
	 * @return
	 */
	public boolean createEmployee(EmployeeDetails employeeDetails) {

		boolean flag = false;
		//calling the method to establish connection
		Connection con = DBUtil.getconn();
		
		//executing insert query by using prepared statement 
		String sql = "insert into Employee_tab(role,emp_name,emp_password,emp_answer) values(?,?,?,?)";
		PreparedStatement st;
		try {
			st = con.prepareStatement(sql);
			
			//setting the values into table
			st.setString(1, employeeDetails.getRole());
			st.setString(2, employeeDetails.getEmployeeName());
			st.setString(3, employeeDetails.getEmployeePassword());
			st.setString(4, employeeDetails.getSecurityQuestion());
			int res = st.executeUpdate();
			if(res > 0 ) {
				flag = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return flag;

	}
	
	/**
	 * This method is used to fetch employee details
	 * @return list of employee details
	 */
	public static List<EmployeeDetails> fetchEmployeeDetails()
	{
		Connection connection = DBUtil.getconn();
		ResultSet resultSet = null;
		Statement statement=null;
		List<EmployeeDetails> employeeDetails = new ArrayList<EmployeeDetails>();
		try {
			//initializing statement object
			statement = connection.createStatement();
			//calling method to execute query
			resultSet=statement.executeQuery("select * from employee_tab");
			//adding details fetched from database to the list
			while(resultSet.next())
			{
				EmployeeDetails details = new EmployeeDetails();
				details.setRole(resultSet.getString(1));
				details.setEmployeeId(resultSet.getInt(2));
				details.setEmployeeName(resultSet.getString(3));
				details.setEmployeePassword(resultSet.getString(4));
				details.setSecurityQuestion(resultSet.getString(5));
				employeeDetails.add(details);
			}
		} catch (Exception e) {

		}
		finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//returning list
		return employeeDetails;	
	}
	

	/**
	 * This method is used for deleting employee when required.This is a manager operation.
	 * @param id
	 * @param role
	 * @return true if the employee deleted successfully else false
	 */
	public boolean deleteEmployee(int id, String role) {
		
		//initializing the variables 
		Connection con = null;
		//calling a method to establish connection
		con = DBUtil.getconn();
		//creating object for prepared statement
		PreparedStatement st;
		String sql = "";
		
		//checking for the employee to who needs to be deleted
		if (role.equalsIgnoreCase("stocker")) {
			sql = "delete from employee_tab where emp_id = ? and role='Stocker'";
		} else {
			sql = "delete from employee_tab where emp_id = ? and role='biller'";
		}

		try {
			st = con.prepareStatement(sql);
			st.setInt(1, id);
			//executing the delete query
			int result = st.executeUpdate();
			if (result>0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	
	

}
