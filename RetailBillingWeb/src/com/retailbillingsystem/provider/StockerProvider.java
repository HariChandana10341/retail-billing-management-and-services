 package com.retailbillingsystem.provider;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.services.StockerServices;
/**
 * This class is used to provide implementations to stocker
 * @author BatchA
 *
 */

@Path("/stocker")
public class StockerProvider{
	

	
	@Path("/addProducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	/**
	 * this method is used to add prodcuts to products table
	 * @param products
	 * @return String
	 */
	public Response addProducts(Products products)
	{
		
		//crunchifyResourceConfig.getContainerResponseFilters().add(CORSOriginFilter.class);

		StockerServices stockerServices = new StockerServices();
		//calling the add product method to add the product
		boolean res = stockerServices.addProduct(products);
//		String response = (res ? "Added Successfully" : "Addition failed");
		
		return Response.ok()
				   .status(201)
				   .header("Access-Control-Allow-Origin", "*")
				   .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT,OPTIONS")
				   .entity(products).build();
	}
	
	/**
	 *  this method is used to delete products from products table 
	 * @param products
	 * @return String
	 */
	@DELETE
	@Path("/delProducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delProducts(Products products) {
		
		StockerServices stockerServices = new StockerServices();
		//calling the delete product method to delete the product
		boolean res = stockerServices.deleteProduct(products.getProductname());
		String result =  (res ? "Deleted Successfully" : "Deletion failed");
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .header("Access-Control-Allow-Methods", 
					        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				   .entity(result).build();
	}
	/**
	 * This method is used to display existing products
	 * @return List of products
	 */
	@GET
	@Path("/displayProducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response displayProducts()
	{
		StockerServices stockerServices = new StockerServices();
		//calling the display product method to display the product details
		List<Products> productList = stockerServices.displayProducts();
		//returning the productlist details
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(productList).build();
	}
	
	/**
	 * This method is used to increase quantity of the existing product
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/increaseQuantity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response increaseQuantity(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		
		//calling the update product method to update the product details
		String result = stockerServices.updateProductQuantity(products.getProductname(), products.getQuantity(), "1");
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(result).build();
	}
	
	/**
	 * This method is used to decrease quantity of the existing product 
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/decreaseQuantity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response decreaseQuantity(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		
		//calling the update product method to update the product details
		String result = stockerServices.updateProductQuantity(products.getProductname(), products.getQuantity(), "2");
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(result).build();
	}
	
	/**
	 * This method is used to increase price of the existing product
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/increasePrice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response increasePrice(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		
		//calling the update product method to update the product details
		String result = stockerServices.updateProductPrice(products.getProductname(),products.getPrice(), "1");
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(result).build();
	}
	
	/**
	 * This method is used to decrease price of the existing product 
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/decreasePrice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response decreasePrice(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		
		//calling the update product method to update the product details
		String result = stockerServices.updateProductPrice(products.getProductname(),products.getPrice(), "2");
		return Response.ok()
				   .status(200)
				   .header("Access-Control-Allow-Origin", "*")
				   .entity(result).build();
	}
}