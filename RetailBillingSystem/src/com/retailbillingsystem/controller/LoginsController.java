package com.retailbillingsystem.controller;

import java.util.Scanner;

import com.retailbillingsystem.dao.EmployeeDao;
import com.retailbillingsystem.services.LoginsServices;
import com.retailbillingsystem.util.ForgotUtilites;

/***
 * This class is for controlling the logins of all the employees
 * @author BATCH A
 *
 */
public class LoginsController {
	
	/***
	 * This method is used for manager to get logged in with his valid credentials
	 */
	public void managerLogin()
	{
		//object creation for writing data class
		EmployeeDao data = new EmployeeDao();
		data.createManager();
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		int count=0;
		
		//validating login of manager
		for(int i = 0; i<3 ;i++)
		{
			System.out.println("Enter the username:");
			String username = scanner.nextLine();
			System.out.println("Enter Password :");
			String password = scanner.nextLine();
			if(LoginsServices.managerLogin(username, password))
			{
				System.out.println("Login Successful...");
				System.out.println("\n******************************************************************************\nWelcome Puran!");
				//object creation for manager controller
				ManagerController controller = new ManagerController();
				controller.managerServices();
				count=1;
				return;
			}
			else
			{
				System.out.println("Invalid credentials!!");
				System.out.println("You have only "+(2-i)+" attempts left!");
			}
		}
		
		//checking whether user want to regain his password
		if(count==0)
		{
			System.out.println("did you forget your password\n1.yes\n2.no");
			String choice=scanner.nextLine();
			if(choice.equals("1"))
			{
				int count1=0;
				for(int i = 0; i<3 ;i++)
				{
					
					//reading the values 
					System.out.println("enter your  registered user id ");
					int id=Integer.parseInt(scanner.nextLine());
					System.out.println("enter your favourite dish");
					String answer=scanner.nextLine();
					
					//calling the forgot password method 
					String password=ForgotUtilites.forgotPasswod(id,answer,"manager");
					if(password!="")
					{
						count1=1;
						System.out.println("your password is: "+ password);
						break;
					}	
					else
					{
						System.out.println("Invalid credentials!!");
						System.out.println("You have only "+(2-i)+" attempts left!");
					}
				}
				if(count1==0)
				{
					System.out.println("too many attempts plese consult manager\n");
					return;
				} 
			
			}
		}
	}
	/***
	 * This method is used for stocker to get logged in with his valid credentials
	 */
	public void stockerLogin()
	{
	
		//validating login of manager
		int count=0;
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		for(int i = 0; i<3 ;i++)
		{
			System.out.println("Enter the username:");
			String username = scanner.nextLine();
			System.out.println("Enter Password :");
			String password = scanner.nextLine();
			if(LoginsServices.stockerLogin(username, password))
			{
				System.out.println("Login Successful");
				System.out.println("\n******************************************************************************\n"+"Welcome "+username+"!");
				//object creation for stocker controller
				StockerController controller = new StockerController();
				controller.Services();
				return;
			}
			else
			{
				System.out.println("Invalid credentials!!");
				System.out.println("You have only "+(2-i)+" attempts left!");
			}
		}
		
			//checking whether user want to regain his password
			if(count==0)
			{
				System.out.println("did you forget your password\n1.yes\n2.no");
				String choice=scanner.nextLine();
			
				if(choice.equals("1"))
				{
					int count1=0;
					for(int i = 0; i<3 ;i++)
					{
						
						System.out.println("enter your  registered user id ");
						int id=Integer.parseInt(scanner.nextLine());
						System.out.println("enter your favourite dish");
						String answer=scanner.nextLine();
						String password=ForgotUtilites.forgotPasswod(id,answer,"stocker");
						if(password!="")
						{
							count1=1;
							System.out.println("your password is: "+ password);
							break;
						}	
						else
						{
							System.out.println("Invalid credentials!!");
							System.out.println("You have only "+(2-i)+" attempts left!");
						}
					}
					if(count1==0)
					{
						System.out.println("too many attempts plese consult manager\n");
						return;
					} 
				
				}
			}
			
		
	}
	
	/***
	 * This method is used for biller to get logged in with his valid credentials
	 */
	public void billerLogin()
	{
		//initializing the variable
		int count=0;
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		for(int i = 0; i<3 ;i++)
		{
			System.out.println("Enter the username:");
			String username = scanner.nextLine();
			System.out.println("Enter Password :");
			String password = scanner.nextLine();
			
			if(LoginsServices.billerLogin(username, password))
			{
				System.out.println("Login Successful");
				System.out.println("\n******************************************************************************\nWelcome "+username+"!");
				//creating the object for biller controller
				BillerController controller = new BillerController();
				controller.billerController();
				return;
			}
			else
			{
				System.out.println("Invalid credentials!!");
				System.out.println("You have only "+(2-i)+" attempts left!");
			}
		}
		
			//checking whether user want to regain his password
			if(count==0)
			{
				System.out.println("did you forget your password  plese select the choice \n1.yes\n2.no");
				String choice=scanner.nextLine();
				while((!choice.equals("1"))&&(!choice.equals("2")))
				{
					System.out.println(" please enter valid choice ");
						choice = scanner.nextLine();
				}
				if(choice.equals("1"))
				{
					int count1=0;
					for(int i = 0; i<3 ;i++)
					{
						int id=0;;
						//reading the values
						System.out.println("enter your  registered user id ");
						try {
						 id=Integer.parseInt(scanner.nextLine());
						} catch (Exception e) {
							System.out.println("oops----!!!!!!!!!!! plese enter integer value only");
							return;
						}
						
						System.out.println("enter your favourite dish");
						String answer=scanner.nextLine();
						String password=ForgotUtilites.forgotPasswod(id,answer,"biller");
						if(password!="")
						{
							count1=1;
							System.out.println("your password is: "+ password);
							break;
						}	
						else
						{
							System.out.println("Invalid credentials!!");
							System.out.println("You have only "+(2-i)+" attempts left!");
						}
					}
					if(count1==0)
					{
						System.out.println("too many attempts plese consult manager\n");
						return;
					} 
				
				}
				
			}
		}
	
	
}
