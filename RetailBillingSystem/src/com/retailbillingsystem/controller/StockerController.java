package com.retailbillingsystem.controller;

import java.util.List;
import java.util.Scanner;

import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.controller.StockerController;
import com.retailbillingsystem.dao.ProductsDao;
import com.retailbillingsystem.services.StockerServices;

/**
 * This class is used for stocker operations
 * @author Batch-A
 *
 */
public class StockerController {
	/**
	 * This method is used to demonstrate all the operations of stocker
	 */
	public void Services() {
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean temp = true;
		StockerController stocker = new StockerController();// creating object for StockerServices class
		String stockerchoice = "";
		//loop to take operations of the stocker
		StockerLoop : while (temp) {
			System.out.println(
					"==============================================================================\n\t\t\tSTOCKER DEPARTMENT\n==============================================================================\n1.Add Stock \n2.Delete stock\n3.update quantity\n4.Update price\n5.displayproducts\n6.Exit\n\r\n"
							+ "------------------------------------------------------------------------------\nWhich operation do you want to perform?");

			try {
				stockerchoice = sc.nextLine();
				System.out.println("==============================================================================");
			} catch (Exception e) {
				System.err.println("Enter only integer values");
				return;
			}

			switch (stockerchoice)// performing operations
			{
			case "1":// Add a stock
				
					stocker.addProduct();
				
					break;
				
			case "2":// delete stock
				

					stocker.deleteProduct();
				
					break;
			case "3":// update quantity
				stocker.updateQuantity();
				break;

			case "4":// update quantity
				stocker.updatePrice();
				break;

			case "5":// display the product list
				try {

					stocker.displayProduct();
				} catch (Exception e) {
					System.err.println("Error!!");
					return;
				}
				break;

			case "6":// to Exit
				temp = false;
				break StockerLoop;

			default:
				System.out.println("Wrong choice \n Enter valid choice");
			
		}

	}
	}

	/**
	 * This method is used to add stock by the stocker
	 */
	public void addProduct()
	{
		//initialising the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		//taking inputs of product details
		System.out.println("Enter the product name:");
		String productname=sc.nextLine();
		
		while((productname.length() == 0) ) {
			System.out.println("enter valid Product name");
			productname = sc.nextLine();
		}
		while(productname.charAt(0) == ' ') {
			System.out.println("enter valid Product name");
			productname = sc.nextLine();
		}
		List<Products> pList=ProductsDao.readFromTable_products();
		for(Products products:pList)
		{
			if(productname.equals(products.getProductname()))
			{
				System.out.println("entered product already exists. Do you want to increase the quantity of this product?\n1.yes\t2.no");
				String tempchoice=sc.nextLine();
				while((!tempchoice.equals("1"))&&(!tempchoice.equals("2")))
				{
					System.out.println("please enter valid choice ");
						tempchoice = sc.nextLine();
				}
				if(tempchoice.equals("1"))
				{
					System.out.println("please enter the quantity");
					double quantity = Double.parseDouble(sc.nextLine());
					StockerServices services = new StockerServices();
					System.out.println(services.updateProductQuantity(productname, quantity,"1"));
					return;
				}
				
				else
				{
					return;
				}
				
			}
		}
		System.out.println("1.Weights\t2.Units");
		System.out.println("Which type of product?");
		String weightchoice=sc.nextLine();
		while((!weightchoice.equals("1"))&&(!weightchoice.equals("2")))
		{
			System.out.println(" please enter valid choice ");
			weightchoice = sc.nextLine();
		}
		String productType="";
		double quantity=0;
		double price=0;
		//adding stock in weights
		if(weightchoice.equals("1"))
		{
			productType="Weights";
			System.out.println("Enter Product quantity:");
			try {
				quantity= Double.parseDouble(sc.nextLine());
			} catch (Exception e) {
				System.err.println("Enter only double values");
				return;
			}
			System.out.println("Enter Product price:");
			try {
				price= Double.parseDouble(sc.nextLine());
			} catch (Exception e) {
				System.err.println("Enter only double values");
				return;
			}
		}
		//adding stock in units
		else if(weightchoice.equals("2"))
		{
			productType = "Units";
			System.out.println("Enter Product quantity:");
			try {
				quantity= Integer.parseInt(sc.nextLine());;
			} catch (Exception e) {
				System.err.println("Enter only Integer values");
				return;
			}
			System.out.println("Enter Product price:");
			try {
				price= Double.parseDouble(sc.nextLine());
			} catch (Exception e) {
				System.err.println("Enter only double values");
				return;
			}
		}
		else
		{
			addProduct();
			return;
		}
		StockerServices services = new StockerServices();
		//calling method to add stock
		if(services.addProduct(productname, quantity, price, productType)) {
			System.out.println("Product added successfully..........");
		}
	}
	/**
	 * This method is used to display all the stock available
	 */
	public void displayProduct()
	{
		StockerServices services = new StockerServices();
		//fetching all the products avaialable
		List<Products> products = services.displayProducts();
		//displaying products
		for(Products product : products)
		{
			System.out.println("Product Name :"+product.getProductname()+" \n"+"Product Quantity: "+product.getQuantity()+"\n"+"Product Price: "+product.getPrice()+"\nProduct Type: "+product.getProductType()+"\n------------------------------------------------------------------------------");
		}
		if(products.size() == 0) {
			System.out.println("----------------THERE ARE NO PRODUCTS IN THE STORE----------------");
		}
	}
	
	/**
	 * This method is used to delete existing stock
	 */
	public void deleteProduct()
	{
		//initialising scanner and taking inputs 
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter product name to be deleted:");
		String productName = sc.nextLine();
		StockerServices services = new StockerServices();
		List<Products> products = services.displayProducts();
		boolean flag=false;
		for(Products product : products)
		{
			if(product.getProductname().equalsIgnoreCase(productName))
			{
				//calling delete product method after finding it
				flag=services.deleteProduct(productName);
			}
		}
		if(flag)
		{
			System.out.println("Product deleted successfully");
		}
		else
		{
			System.out.println("Entered Product not found!!");
		}
	}
	
	/**
	 * This method is used to update existing quantity
	 */
	public void updateQuantity()
	{
		//taking inputs of the product details
		System.out.println("Enter Product whose quantity needs to be updated:");
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String productname = sc.nextLine();
		System.out.println("1.Increase Quantity\t 2.Decrease Quantity");
		
		String operation = sc.nextLine();
		
		StockerServices services = new StockerServices();
		List<Products> products = ProductsDao.readFromTable_products();
	
		boolean flag=false;
		for(Products product : products)
		{
			if(product.getProductname().equalsIgnoreCase(productname))
			{
				flag = true;
				System.out.println("Enter quantity to be updated:");
				double quantity;
				try {
					if(product.getProductType().equalsIgnoreCase("units")) {
						quantity =  Integer.parseInt(sc.nextLine());
					} else {
						quantity =  Double.parseDouble(sc.nextLine());
					}
					
				} catch (Exception e) {
					System.err.println("Enter only valid values!");
					return;
				}
				//calling method to update the product quantity
				System.out.println(services.updateProductQuantity(productname, quantity,operation));
			}
		}
		if(!flag) {
			System.out.println("Entered product not found......");
		}
		
	}
	
	/**
	 * This method is used to update price of an existing product
	 */
	public void updatePrice() {
		//taking input of product details
		System.out.println("Enter Product whose Price needs to be updated:");
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String productname = sc.nextLine();
		System.out.println("1.Increase Price\t 2.Decrease Price");
		
		String operation = sc.nextLine();
		
		StockerServices services = new StockerServices();
		//fetching list of products available
		List<Products> products = ProductsDao.readFromTable_products();
	
		boolean flag=false;
		for(Products product : products)
		{
			if(product.getProductname().equalsIgnoreCase(productname))
			{
				flag = true;
				System.out.println("Enter Price to be updated:");
				double Price;
				try {
					if(product.getProductType().equalsIgnoreCase("units")) {
						Price =  Integer.parseInt(sc.nextLine());
					} else {
						Price = Double.parseDouble(sc.nextLine());
					}
					
				} catch (Exception e) {
					System.err.println("Enter only double values!");
					return;
				}
				//calling method to update product price
				System.out.println(services.updateProductPrice(productname, Price,operation));
			}
		}
		if(!flag) {
			System.out.println("Entered product not found......");
		}
		
	}

}
