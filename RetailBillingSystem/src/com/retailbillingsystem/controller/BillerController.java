package com.retailbillingsystem.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.retailbillingsystem.bean.FeedBack;
import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.InvoiceDao;
import com.retailbillingsystem.dao.ProductsDao;
import com.retailbillingsystem.services.BillerServices;
import com.retailbillingsystem.services.StockerServices;

/***
 * This class is used as controller for biller department
 * @author BATCH A
 *
 */
public class BillerController {

	/***
	 * this method is used to select an option to perform biller operations
	 */
	public void billerController() {
		
		//variable declaration and initialization
		int invoice_num;
		
		double total = 0;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("==============================================================================\n\t\t\tBILLING DEPARTMENT\n==============================================================================\n");
		//respective list creations
		List<Products> productList = ProductsDao.readFromTable_products();
		List<Invoice_details> billingList = new ArrayList<Invoice_details>();
		List<Invoice_details> invoiceDetails=InvoiceDao.readFromTable_invoice();
		//initializing invoice number
		if(invoiceDetails.isEmpty())
		{
			invoice_num=1001;
		}
		else
		{
			invoice_num=invoiceDetails.get(invoiceDetails.size()-1).getInvoiceno(); 
			invoice_num++;
		}
		
		System.out.println("the products that are avilable in our market are ");
		System.out.println("------------------------------------------------");
		StockerController controller = new StockerController();
		controller.displayProduct();
		
		//options for bill generation
		while(true) {
			int count = 0;
			System.out.println("choose an option.\n 1. Add product\n 2. Generate bill");
			String option = sc.nextLine();
			if (!(option.equals("1")) && !(option.equals("2"))) {
				System.out.println("\nEnter valid choice!\n");
				continue;
			}

			if (option.equals("2")) {
				if (billingList.size() == 0) {
					System.out.println("You have not Entered any product in the cart!!!");
					return;
				}
				System.out.println("terminated!!!");
				break;
			}
			
			System.out.println("Enter the product name:");
			String pname = sc.nextLine();
			for (Products product : productList) {
				
				//checking the equals condition of productName with existing product name
				if (pname.equalsIgnoreCase(product.getProductname())) {
					double quantity = 0;
					
					count++;
					System.out.println("Enter the product quantity:");
					try {
						//checking  for the type of quantity
						if(product.getProductType().equalsIgnoreCase("units")) {
						quantity = Integer.parseInt(sc.nextLine());
						while (quantity < 0) {
							System.out.println("Enter valid quantity!");
							quantity = Integer.parseInt(sc.nextLine());
						}
						}else {
							System.out.println("Which type of quantity you want to give?\n1.Kgs\t2.Grams");
							String weightType = sc.nextLine();
							if (!(weightType.equals("1")) && !(weightType.equals("2"))) {
								System.out.println("\nEnter valid choice!\n");
								continue;
							}
							
							System.out.println("Enter quantity:");
							quantity = Double.parseDouble(sc.nextLine());
							if(weightType.equals("2"))
							{
								quantity=(double)quantity/1000;
							}
							
							while (quantity < 0) {
								System.out.println("Enter valid quantity!");
								quantity = Double.parseDouble(sc.nextLine());
						}
						}
					} catch (Exception e) {
						System.err.println("Enter only numeric values!!");
						return;
					}
					//stocker services object creation
					StockerServices stockservices = new StockerServices();
					//calling method for updating product quantity
					String approval = stockservices.updateProductQuantity(pname , quantity ,"2");
					// Calculating total
					if (approval.equals("Product Updated Successfully......")) {
						BillerServices billerServices = new BillerServices();
						
						double price = billerServices.price_calculation(pname, quantity);
						total = billerServices.total_calculation(price, total);
						Invoice_details billingproduct = new Invoice_details();
						// set products details
						billingproduct.setProduct_name(product.getProductname());
						billingproduct.setInvoiceno(invoice_num);
						billingproduct.setPrice(price);
						billingproduct.setQuantity(quantity);
						billingList.add(billingproduct);
					} else {
						System.out.println(approval+"\n");
					}
				}
				
			}	
			if(count == 0) {
				System.out.println("--------------PRODUCT NOT FOUND---------------");
			}
		}

		//creating object for biller services
		BillerServices billerServices = new BillerServices();
		billerServices.invoiceStoring(invoice_num, total, billerServices.discount_calculation(total), billerServices.tax_caluculation(total));
		billerServices.billingProducts((ArrayList<Invoice_details>) billingList);
		List<Products> billingProducts = ProductsDao.readFromTable_billingProducts(invoice_num);
		displaybill(billingProducts, total);
		customer_feedback();
	}
	
	/***
	 * This method is for diplaying bill after being generated
	 * @param displayList
	 * @param total
	 */
	public void displaybill(List<Products> displayList , double total)
	{
		//creating object for date class
		Date date = new Date();
	// displaying the total bill
	System.out.println(
			"\n*****************************************************************************\n\t\tINNOMINDS QUALITY STORE\n*****************************************************************************\n\tDate:"+date);
	System.out.printf("%10s %20s %15s", " Productname", "productquantity", "productprice");
	System.out.println(
			"\n========================================================================================================");

	for (Products product : displayList) {
		System.out.printf("%10s %20s %10s", product.getProductname(), product.getQuantity(), product.getPrice());
		System.out.println(
				"\n----------------------------------------------------------------------------------------------------------");
	}
	BillerServices billerServices = new BillerServices();
	double tax = billerServices.tax_caluculation(total);
	// discount calculation
	double discount = billerServices.discount_calculation(total);
	if (total >= 500) {
		discount = ((double) 7 / 100) * total;
		total -= discount;
	}

	System.out.println("\tTax for this bill is:            " + tax);
	if (discount != 0) {
		System.out
				.println("\tYou have recieved a discount:    " + discount + "\n\tfor shopping above 500rs.");
	}
	System.out.println("\n\tTotal price is:                  " + total);
	
	}
	
	/***
	 * this method is for Storing customer feedback
	 */
	
	public void customer_feedback()
	{
			
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			System.out.println("\nPlease give your feedback:\n1.Very Good\n2.Good\n3.Average\n4.Poor\n5.Very poor\n");
			//asking user inputs
	        int feedback=0;
	        String mobilenumber="";
	        boolean flag=false;
	        try {
	        	//feedback validation
	        
	    	   feedback = Integer.parseInt(sc.nextLine());
	          while((feedback <  1) && (feedback > 5)) {
	        	  System.out.println("please enter valid feedback");
	        	  feedback = Integer.parseInt(sc.nextLine());
	          }
	        } catch (Exception e) {
			    System.err.println("Enter correct choices");
				customer_feedback();
			}
	        //creating date object
	  		Date date1 = new Date();
	  		//converting the date type into string  by using toString
            String date=date1.toString();
          //feedback validation
			FeedBack costumerFeedback = new FeedBack();
			costumerFeedback.setFeedback(feedback);		
			costumerFeedback.setDateandtime(date);
	
			if(!flag) {
				System.out.println("Enter your mobile number");
			 mobilenumber = sc.nextLine();
			 flag=BillerServices.mobileCheck(mobilenumber);
			 if(flag)
			 {
				 costumerFeedback.setMobileNumber(mobilenumber);
			 }
			 else {
				 System.out.println("oops!!!!!!plese enter valid mobile number");
				 customer_feedback();
				
				 return;
			 }
			}
			BillerServices.customerFeedback(costumerFeedback);
			System.out.println("=========================THANK YOU FOR YOUR FEEDBACK=====================");
			}
}