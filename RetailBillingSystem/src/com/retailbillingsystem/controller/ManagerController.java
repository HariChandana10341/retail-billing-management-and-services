package com.retailbillingsystem.controller;


import java.util.List;
import java.util.Scanner;

import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.bean.FeedBack;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.EmployeeDao;
import com.retailbillingsystem.dao.FeedbackDao;
import com.retailbillingsystem.dao.InvoiceDao;
import com.retailbillingsystem.dao.ProductsDao;
import com.retailbillingsystem.services.*;

/***
 * This class is used for controlling the manager operations
 * @author BATCH A
 *
 */
public class ManagerController {

	/***
	 * This method is used for controlling the manager operation choices
	 */
	public void managerServices() {

		//intializing the variable  
		boolean temp = true;
		String managerchoice = "";

		
		ManagerController manager = new ManagerController();// creating objects for ManagerServices class
		ManagerLoop : while (temp) {
			System.out.println(
					"==============================================================================\n\t\t\tMANAGER DEPARTMENT\n==============================================================================\n1.Add an Employee\n2.Delete an Employee\n3.Display Employees \n4.Display Products\n5.View feedback of store\n6.View previous bills\n7.Exit\n==============================================================================\nWhich operation do you want to perform?");

			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			
				managerchoice = sc.nextLine();
				System.out.println("                                                    ");
			
			// manager's operation choices
			switch (managerchoice)// performing operations
			{
			case "1":// addEmployee
				manager.addEmployee();

				break;

			case "2":// Delete a employee

				manager.deleteEmployee();

				break;
			case "3":// Display employees
				manager.displayEmployee();
				break;

			case "4":// Display products
				manager.displayProducts();
				break;

			case "5":// Display feedback

				manager.displayfeedback();
				break;

			case "6":// Display previousbills
				manager.displayPreviousbills();
				break;

			case "7":// exit from menu
				temp = false;

				break ManagerLoop;

			default:
				System.out.println("Wrong choice\nEnter valid choice");
			}
			

		}
	}

	/***
	 * This method is used for adding employees into data base
	 */
	public void addEmployee() {
		//reading the values 
		
		System.out.println("1.Stocker\t2.Biller");
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("enter the choice ");
		String choice = s.nextLine();
		while((!choice.equals("1"))&&(!choice.equals("2")))
		{
			System.out.println(" please enter valid choice ");
				choice = s.nextLine();
		}
		boolean flag = false;
		ManagerService managerServices = new ManagerService();
		//switch condition for verifying the employee
		switch (choice) {
		//adding stocker
		case "1":
			
			System.out.println("enter the Stocker name");
			String stockerName = s.nextLine();
			while((stockerName.length() == 0) ) {
				System.out.println("enter valid Stocker name");
				stockerName = s.nextLine();
			}
			while(stockerName.charAt(0) == ' ') {
				System.out.println("enter valid Stocker name");
				stockerName = s.nextLine();
			}
			
			boolean check=false;
			while(!check)
			{
				System.out.println("enter the password of the stocker");
				String stockerPassword = s.nextLine();
				//method for password verification
				check = ManagerService.passwordChecking(stockerPassword);
				if(check)
				{
					System.out.println("what is your favorite dish?");
					String sdish = s.nextLine();
					
					while((sdish.length() == 0) ) {
						System.out.println("enter valid Answer");
						sdish = s.nextLine();
					}
					while(sdish.charAt(0) == ' ') {
						System.out.println("enter valid Answer");
						sdish = s.nextLine();
					}
					
					//calling method to add stocker
					flag = managerServices.createStocker(stockerName, stockerPassword, sdish);
					if(flag) {
						System.out.println("Employee added sucessfully......");
						List<EmployeeDetails> stockerList=EmployeeDao.fetchEmployeeDetails();
						System.out.println("your employee id is  "+stockerList.get(stockerList.size()-1).getEmployeeId());
					}
					
				}
				else
				{
					System.out.println("your password should contain atleast 1 number");
				}
			}
			
			break;
		//adding biller
		case "2":
			System.out.println("enter the Biller name");
			String billerName = s.nextLine();
			
			while((billerName.length() == 0) ) {
				System.out.println("enter valid Answer");
				billerName = s.nextLine();
			}
			while(billerName.charAt(0) == ' ') {
				System.out.println("enter valid Answer");
				billerName = s.nextLine();
			}
			boolean check1=false;
			while(!check1)
			{
				System.out.println("enter the password of the biller");
				String billerPassword = s.nextLine();
				//method for password verification
				check1=ManagerService.passwordChecking(billerPassword);
				if(check1)
				{
					System.out.println("what is your favorite dish?");
					String bdish = s.nextLine();
					
					while((bdish.length() == 0) ) {
						System.out.println("enter valid Answer");
						bdish = s.nextLine();
					}
					while(bdish.charAt(0) == ' ') {
						System.out.println("enter valid Answer");
						bdish = s.nextLine();
					}
					//calling method to add stocker
					flag = managerServices.createBiller(billerName,billerPassword,bdish);
					if(flag) {
						System.out.println("Employee added sucessfully......");
						List<EmployeeDetails> billerList=EmployeeDao.fetchEmployeeDetails();
						System.out.println("your employee id is  "+billerList.get(billerList.size()-1).getEmployeeId());
					}
				}
				else
				{
					System.out.println("your password should contain atleast 1 number");
				}
			}
			break;
			
		default:
			System.out.println("enter valid choice\n");

		}
	}

	/***
	 * This method is used for deleting the employees from the database
	 */
	public void deleteEmployee() {
		//reading the values 
		System.out.println("1.Stocker\t2.Biller");
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("enter the choice ");
		String choice = s.nextLine();
		while((!choice.equals("1"))&&(!choice.equals("2")))
		{
			System.out.println(" please enter valid choice ");
				choice = s.nextLine();
				
		}
		
		ManagerService managerServices = new ManagerService();
		String role="";
		//switch condition for verifying the employee
		switch (choice) {
		//deleting stocker
		case "1":
			boolean flag=false;
			int count=0;
			int stockerid=0;
			while(!flag) {
			System.out.println("enter  valid Stocker ID");
			int value=0;
			try {
				String id= s.nextLine();
				for(int i=0;i<id.length();i++)
				{
					
					if(id.charAt(i)>=48 && id.charAt(i)<=57)
					{
						count++;
					}
				}
				if(count==id.length()) {
				      flag=true;
					for(int i=0;i<id.length();i++)
					{
					   value = value * 10+ (id.charAt(i)-48);
					}
				}
				if(flag) {
				stockerid=value;
				}
			}
				
			 catch (Exception e) {
				System.err.println("Enter integer values!!");
				return;
			}
			}
			
			role="Stocker";
			//calling method to delete
			if(managerServices.deleteEmployee(stockerid,role))
			{
				System.out.println("Stocker deleted successfully");
			}
			else {
				System.out.println("employee id not found to delete ");
			}
			break;
		//deleting biller
		case "2":
	
			boolean flag1=false;
			int count1=0;
			int billerid=0;
			while(!flag1) {
			System.out.println("enter valid biller ID ");
			int value=0;
			try {
				String id= s.nextLine();
				for(int i=0;i<id.length();i++)
				{
					
					if(id.charAt(i)>=48 && id.charAt(i)<=57)
					{
						count1++;
					}
				}
				if(count1==id.length()) {
				      flag1=true;
					for(int i=0;i<id.length();i++)
					{
					   value = value * 10+ (id.charAt(i)-48);
					}
				}
				if(flag1) {
				billerid=value;
				}
			}
				
			 catch (Exception e) {
				System.err.println("Enter integer values!!");
				return;
			}
			}
			role="Biller";
			//calling method to delete
			if(managerServices.deleteEmployee(billerid,role))
			{
				System.out.println("biller deleted successfully");
			}
			else {
				System.out.println("employee id not found to delete  ");
			}
			break;
			
			
		default:
			System.out.println("enter valid choice\n");

		}

	}
	/***
	 * This method is for displaying bills
	 */
	public void displayPreviousbills() {
		//taking list of invoice numbers
		List<Integer>invoicenumbers = InvoiceDao.readFromTable_InvoicenNumbers();
		if(invoicenumbers.size() == 0) {
			System.out.println("----------THERE ARE NO PREVIOUS RECORDS-----------");
			return;
		}
		//displaying previous invoice numbers 
		System.out.println("invoice  numbers are:");
		for(int i=0;i<invoicenumbers.size();i++)
		{
			System.out.println(invoicenumbers.get(i));
		}
		
		double total=0;
		//taking invoice number input
		System.out.println("enter the invoice number");
		int invoiceNo;
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		try
		{
			invoiceNo=Integer.parseInt(sc.nextLine());
			int count=0;
			 for(int i=0;i<invoicenumbers.size();i++)
			 {
				 if(invoiceNo==invoicenumbers.get(i))
				 {
					count++;
				 }
			 }
			 if(count==0)
			 {
				 System.out.println("invoice number not found");
				 displayPreviousbills();
				 return;
			 }
		}catch (Exception e) {
			System.err.println("plse enter integer only");
			return;
		}
		BillerServices billerServices=new BillerServices();
		//calling method to dispaly bill of invoice number given
		List<Products> invoiceList = ProductsDao.readFromTable_billingProducts(invoiceNo);
           for(Products products:invoiceList)
           {
        	 total= billerServices.total_calculation(products.getPrice(),total);
           }
           BillerController billerController=new BillerController();
           billerController.displaybill(invoiceList, total);
	}

	/**
	 * This method is used to display previous feedbacks
	 */
	public void displayfeedback() {
		FeedbackDao feedback = new FeedbackDao();
		//Fetching feedback details
		List <FeedBack> feedbackdetails = feedback.fetchFeedBackDetails();
		String Feedback;
		System.out.println("\t\t\tSTORE FEED BACKS\t\t\t");
		System.out.println("===================================================");
		for(FeedBack details :feedbackdetails) {
			if(details.getFeedback()==1) {
				Feedback="very good";
			}
			else if(details.getFeedback()==2) {
				Feedback= "good";
			}
           else if(details.getFeedback()==3) {
        	   Feedback= "average";
			}
           else if(details.getFeedback()==4) {
        	   Feedback ="poor";
			}
           else {
        	   Feedback=" very poor";
           }
		System.out.println("Date and Time: "+details.getDateandtime()+"  "+" \nFeedback: "+ Feedback +" \nMobile number : "+details.getMobileNumber()+"\n------------------------------------------------------------------------------");
		}
	}
	/**
	 * This method is used to display products
	 */
	public void displayProducts() {
		StockerController controller= new StockerController();
				controller.displayProduct();

	}

	/***
	 * This method is used for displaying employees when required
	 */
	public void displayEmployee() {
		ManagerService managerservice=new ManagerService();
		//Fetching employee details
		List <EmployeeDetails> employeelist= managerservice.displayEmployees();
		//Taking values
		System.out.println("1.Stocker employees\t2.Biller Employees");
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		String choice = scanner.nextLine();
		while((!choice.equals("1"))&&(!choice.equals("2")))
		{
			System.out.println(" please enter valid choice ");
				choice = scanner.nextLine();
		}
		
		boolean flag = false;
		//displaying employees
	   for(EmployeeDetails details :employeelist) {
		   if(choice.equals("1") && details.getRole().equalsIgnoreCase("Stocker"))
		   {
			   System.out.println("Employee Name :"+details.getEmployeeName()+" \n"+"Employee Id: "+details.getEmployeeId()+"\n"+"\n------------------------------------------------------------------------------");
			   flag = true;
		   }
		   else if(choice.equals("2") && details.getRole().equalsIgnoreCase("Biller"))
		   {
			   System.out.println("Employee Name :"+details.getEmployeeName()+" \n"+"Employee Id: "+details.getEmployeeId()+"\n"+"\n------------------------------------------------------------------------------");
			   flag = true;
		   }
		   
	   }if(!flag) {
		   System.out.println("----------------NO EMPLOYEE EXISTS-----------------");
	   }
	   

	}

}
