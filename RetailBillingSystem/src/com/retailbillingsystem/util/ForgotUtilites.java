package com.retailbillingsystem.util;

import java.util.List;
import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.dao.EmployeeDao;
/**
 * This method is used for getting password details
 * @author Batch-A
 *
 */
public class ForgotUtilites {
    /**
     * This method is used for getting password details from database table
     * @param id
     * @param dish
     * @param role
     * @return password 
     */
	public static String forgotPasswod(int id,String dish,String role)
	{
		String password="";
		List<EmployeeDetails> employeeDetails = EmployeeDao.fetchEmployeeDetails();
		// checking details and getting password from table
		for(EmployeeDetails  employee: employeeDetails)
		{
			if((id==employee.getEmployeeId())&&(dish.equals(employee.getSecurityQuestion())&&(employee.getRole().equalsIgnoreCase(role)))) {
			     password=employee.getEmployeePassword();
			}
			
			
		}
		
		return password;
		
	}
}