create database retailbilling;
show databases;
use retailbilling;
show tables;
desc employee_tab;
create table Employee_tab(role varchar(20) not null, emp_id int primary key auto_increment,emp_name varchar(20) not null,emp_password varchar(20) not null,emp_answer varchar(20) not null);
create table Products_tab(product_id int primary key auto_increment,product_name varchar(20) not null,product_quantity double not null,product_price double not null,quantity_type varchar(20) not null);
desc products_tab;
create table customerfeedback_tab(date_time varchar(50) not null,feedback  varchar(10) not null,cst_mbn varchar(15),feedback_id int auto_increment primary key);

create table invoice_tab(Invoicenumber int,product_name varchar(50),product_quantity double not null,product_price double not null, primary key(Invoicenumber,product_name));
desc invoice_tab;
create table biller_tab(Invoicenumber int primary key references invoice_tab,discount double,tax double,total double);
desc biller_tab;

